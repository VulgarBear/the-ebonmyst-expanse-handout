<div align="center">

![Splash Image](./assets/img/splash-banner.png)

![GitLab all issues](https://img.shields.io/gitlab/issues/all/VulgarBear%2Fthe-ebonmyst-expanse-handout?gitlab_url=https%3A%2F%2Fgitlab.com%2F&style=for-the-badge)
![GitLab (self-managed)](https://img.shields.io/gitlab/license/VulgarBear%2Fthe-ebonmyst-expanse-handout?gitlab_url=https%3A%2F%2Fgitlab.com%2F&style=for-the-badge)
![GitLab Release (by release name)](https://img.shields.io/gitlab/v/release/VulgarBear%2Fthe-ebonmyst-expanse-handout?gitlab_url=https%3A%2F%2Fgitlab.com%2F&style=for-the-badge)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/VulgarBear%2Fthe-ebonmyst-expanse-handout?gitlab_url=https%3A%2F%2Fgitlab.com%2F&style=for-the-badge)


</div>
<div align="center">

# The Ebonmyst Expanse Handout

</div>

The Ebonmyst Expanse is a homebrew Pathfinder 2e adventure made for use with my personal games. This repo server as the versioning for my [GMBinder][gmbinder] document. Useage of this document and it's content are free for others to use. As more is known about the adventure aditional information may be provided here.

The template was supplied by Apostol Apostolov's template on GMBinder Pathfinder 2e template.

<!-- Links -->
[gmbinder]: https://www.gmbinder.com/